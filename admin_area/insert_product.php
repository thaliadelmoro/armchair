<!DOCTYPE>
<?php
require("includes/db.php");
?>

<html>
	<head>
		<title>Inserting Product</title>
		<link rel="stylesheet" href="/armchair/styles/style.css" media="all" />
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">		
		<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-2020.css">
	</head>
<body class="w3-content w3-margin-top" >

<div class="w3-card-4">
<div class="w3-container  w3-2020-faded-denim">
  <h2>Insert New Product</h2>
	</div>
	
		<form class="w3-container w3-white" action="insert_product.php" method="post" enctype="multipart/form-data" id="usrform">
		
			<label>Product Title</label>
			<input class="w3-input w3-border" type="text" name="product_title" required>
			
			<label>Product Category</label>
			
			<select name="product_cat" class="w3-input w3-border"  required>
					<option> Select a Category</option>
					<?php
					$get_cats="select * from category";
					$run_cats=mysqli_query($con, $get_cats);
					While ($row_cats=mysqli_fetch_array($run_cats)){
						$cat_id=$row_cats['category_id'];
						$cat_title=$row_cats['category_title'];
					Echo"<option value='$cat_id'>$cat_title</option>";
					}
	
					?>
			</select>
					
			<label>Product Image</label>
			<input class="w3-input" type="file" name="product_image">
			
			<label>Product Price</label>
			<input class="w3-input w3-border" type="text" name="product_price">
			
			<label>Product Description</label>
			<textarea form="usrform" class="w3-input w3-border" type="textarea" name="product_desc"></textarea>
	
			<label>Product Keyword</label>
			<input class="w3-input w3-border" type="text" name="product_keyword">
		
			<input class="w3-btn w3-2020-faded-denim w3-section" type="submit" name="insert_post" value="Insert Now" />
	
		</form>
	</div>
</div>
</div>
</body>
</html>

<?php
	If(isset($_POST['insert_post']))
	{

		//getting the text data from the fields
		$product_title=$_POST['product_title'];
		$product_cat=$_POST['product_cat'];
		$product_price=$_POST['product_price'];
		$product_desc=$_POST['product_desc'];
		$product_keywords=$_POST['product_keyword'];
	
		//getting the image from the field
		$product_image=$_FILES['product_image']['name'];
		$product_image_tmp=$_FILES['product_image']['tmp_name'];
		
		move_uploaded_file($product_image_tmp,"product_images/$product_image");
	
		//To push the data to the database
		echo $insert_product="insert into product (category_id, product_name, price, product_desc, product_image, product_keywords ) values ('$product_cat','$product_title','$product_price','$product_desc','$product_image','$product_keywords')";
		
		$insert_pro= mysqli_query($con, $insert_product);
		if($insert_pro){
			echo"<script>alert('Product has been inserted!')</script>";
			echo"<script>windows.open('insert_product.php','_self')</script>";
		} else {
			echo"<script>alert('Failed.')</script>";
		}
	}
?>