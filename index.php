<!DOCTYPE html>
<?php
Include("functions/functions.php");
?>

<html>
	<head> 
		<title>armchair</title>
		<link rel="stylesheet" href="/armchair/styles/style.css" media="all" />
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>

<body class="w3-content" style="max-width:3000px">
<div>
 <!--Header starts here-->   
	<div class="header">
		<a href="index.php" class="logo">armchair.</a>
		<div class="header-right">
			<a class="active" href="index.php">Home</a>
			<a href="shop_chairs.php">Chairs</a>
			<a href="shop_lamps.php">Lightings</a>
			<a href="shop_tables.php">Tables</a>
			<a href="shop_collections.php">Collections</a>
			<a href="#contact">Contact</a>
			<a href="about.php">About</a>
			<a href="cart.php"><i class="fa fa-shopping-cart" style="font-size:25px;"></i></a>
		</div>
	</div>
	
<!-- !PAGE CONTENT! -->
<div class="w3-main" >
	 
	<!-- Product #1 -->
	<div class="w3-display-container w3-container w3-section">
		<img src="/armchair/images/chair.jpg" alt="Chairs" style="width:100%">
		<div class="w3-display-topleft" style="padding:230px 230px">
			<h1 class="w3-jumbo w3-hide-medium">New Arrival Modern Chair</h1>
			<h1 class="w3-hide-large w3-hide-medium">Modern Chair</h1>
		     <h1 class="w3-hide-medium">The latest collection from lorem ipsum dolor</h1>
		     <h1 class="w3-hide-jumbo w3-hide-large ">Modern Chair</h1>
		     <p><a href='shop_chairs.php'><button class="w3-button w3-padding-large w3-large w3-btn w3-black w3-ripple">SHOP NOW</button></a></p>
		</div>
	</div>
	
	<!-- Product #2-->
	<div class="w3-display-container w3-container w3-section">
		<img src="/armchair/images/bed.jpg" alt="Chairs" style="width:100%">
		<div class="w3-display-topleft" style="padding:230px 230px">
			<h1 class="w3-jumbo w3-hide-medium">Beds</h1>
			<h1 class="w3-hide-large w3-hide-medium">Comfy Bed</h1>
		     <h1 class="w3-hide-medium">The latest addition from the best designers you must have.</h1>
		     <h1 class="w3-hide-jumbo w3-hide-large ">Beds</h1>
		     <p><a href='shop_collections.php'><button class="w3-button w3-padding-large w3-large w3-btn w3-black w3-ripple">SHOP NOW</button></a></p>
		</div>
	</div>
	
	<!-- Product #3-->
	<div class="w3-display-container w3-container w3-section">
		<img src="/armchair/images/table.jpg" alt="Chairs" style="width:100%">
		<div class="w3-display-topleft" style="padding:230px 230px">
			<h1 class="w3-jumbo w3-hide-medium">Dinning Tables</h1>
			<h1 class="w3-hide-large w3-hide-medium">Dinning Tables</h1>
		     <h1 class="w3-hide-medium">The latest addition from the best designers you must have.</h1>
		     <h1 class="w3-hide-jumbo w3-hide-large ">Tables</h1>
		     <p><a href='shop_tables.php'><button class="w3-button w3-padding-large w3-large w3-btn w3-black w3-ripple">SHOP NOW</button></a></p>
		</div>
	</div>

	<!-- Product #3-->
	<div class="w3-display-container w3-container w3-section">
		<img src="/armchair/images/lamp.jpg" alt="Chairs" style="width:100%">
		<div class="w3-display-topleft" style="padding:230px 230px">
			<h1 class="w3-jumbo w3-hide-medium">Lightings</h1>
			<h1 class="w3-hide-large w3-hide-medium">Lightings</h1>
		     <h1 class="w3-hide-medium">Give brightness to your living space.</h1>
		     <h1 class="w3-hide-jumbo w3-hide-large ">Lightings</h1>
		     <p><a href='shop_lamps.php'><button class="w3-button w3-padding-large w3-large w3-btn w3-black w3-ripple">SHOP NOW</button></a></p>
		</div>
	</div>

<!--Footer starts here--> 
  <!-- Footer -->
  <footer class="w3-padding-64 w3-light-grey w3-small w3-center" id="footer">
    <div class="w3-row-padding">
      <div class="w3-col s4">
        <h4>Contact</h4>
        <p>Questions? Go ahead.</p>
        <form action="/action_page.php" target="_blank" id="contact">
          <p><input class="w3-input w3-border w3-margin" type="text" placeholder="Name" name="Name" required></p>
          <p><input class="w3-input w3-border w3-margin" type="text" placeholder="Email" name="Email" required></p>
          <p><input class="w3-input w3-border w3-margin" type="text" placeholder="Subject" name="Subject" required></p>
          <p><input class="w3-input w3-border w3-margin" type="text" placeholder="Message" name="Message" required></p>
          <button type="submit" class="w3-button w3-block w3-black w3-margin">Send</button>
        </form>
      </div>

      <div class="w3-col s4">
        <h4>About</h4>
        <p class="w3-margin-top"><a href="#">About us</a></p>
        <p class="w3-margin-top"><a href="#">We're hiring</a></p>
        <p class="w3-margin-top"><a href="#">Support</a></p>
        <p class="w3-margin-top"><a href="#">Find store</a></p>
        <p class="w3-margin-top"><a href="#">Shipment</a></p>
        <p class="w3-margin-top"><a href="#">Payment</a></p>
        <p class="w3-margin-top"><a href="#">Return</a></p>
        <p class="w3-margin-top"><a href="#">Help</a></p>
      </div>

      <div class="w3-col s4 w3-justify">
        <h4>Store</h4>
        <p><i class="fa fa-fw fa-map-marker w3-margin-top"></i> Imus St. Imus City Cavite, 4103</p>
        <p><i class="fa fa-fw fa-phone w3-margin-top"></i> 0012345678</p>
        <p><i class="fa fa-fw fa-envelope w3-margin-top"></i> contact@archair.com</p>
        <h4>We accept</h4>
        <p><i class="fa fa-fw fa-cc-amex w3-margin-top"></i> Mastercard</p>
        <p><i class="fa fa-fw fa-credit-card w3-margin-top"></i> Visa</p>
        <br>
        <i class="fa fa-facebook-official w3-hover-opacity w3-large"></i>
        <i class="fa fa-instagram w3-hover-opacity w3-large"></i>
        <i class="fa fa-snapchat w3-hover-opacity w3-large"></i>
        <i class="fa fa-pinterest-p w3-hover-opacity w3-large"></i>
        <i class="fa fa-twitter w3-hover-opacity w3-large"></i>
        <i class="fa fa-linkedin w3-hover-opacity w3-large"></i>
      </div>
    </div>
  </footer>

<!-- End page content -->
</div>
</html>
