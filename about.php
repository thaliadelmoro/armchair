<!DOCTYPE html>
<?php
Include("functions/functions.php");
?>

<html>
	<head> 
		<title>armchair</title>
		<link rel="stylesheet" href="/armchair/styles/style.css" media="all" />
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>

<body class="w3-content" style="max-width:3000px">
<div>
 <!--Header starts here-->   
	<div class="header">
		<a href="index.php" class="logo">armchair.</a>
		<div class="header-right">
			<a href="index.php">Home</a>
			<a href="shop_chairs.php">Chairs</a>
			<a href="shop_lamps.php">Lightings</a>
			<a href="shop_tables.php">Tables</a>
			<a href="shop_collections.php">Collections</a>
			<a href="#contact">Contact</a>
			<a class="active" href="about.php">About</a>
			<a href="cart.php"><i class="fa fa-shopping-cart" style="font-size:25px;"></i></a>
		</div>
	</div>
	
<!-- !PAGE CONTENT! -->
<!-- Page content -->
<div class="w3-content" style="max-width:1100px">

  <!-- About Section -->
  <div class="w3-row w3-padding-64" id="about">
    <div class="w3-col m6 w3-padding-large w3-hide-small">
     <img src="/armchair/images/about2.jpg" class="w3-round w3-image w3-opacity-min" width="600" height="750">
    </div>

    <div class="w3-col m6 w3-padding-large">
      <h1 class="w3-center">Who we are</h1>
      <h4 class="w3-center">Tradition since 1997</h4>
      <p class="w3-large">Founded in blabla by Mr. Dalisay in lorem ipsum dolor sit amet, consectetur adipiscing elit consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute iruredolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
      <p class="w3-large w3-text-grey w3-hide-medium">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
  </div>
  
  <hr>
  
  <!-- Products Section -->
  <div class="w3-row w3-padding-64" id="menu">
    <div class="w3-col l6 w3-padding">
      <h1 class="w3-center">What we do</h1><br>
      <h4>Fast & Free Shipping </h4>
      <p class="w3-text-grey">Every single order ships for free. </p><br>
    
      <h4>Modular, easy to move design</h4>
      <p class="w3-text-grey">Our innovative modular design is driven by the belief that furniture should be lorem ipsum. Must have for a modern Filipino home.</p><br>
    
      <h4>Durable, premium materials</h4>
      <p class="w3-text-grey">We use materials like sustainably-forested wood, strengthened steel hardware, and top-grain Philippine leather.</p><br>
    
      </div>
    
    <div class="w3-col l6 w3-padding-large">
      <img src="/armchair/images/about1.jpg" class="w3-round w3-image w3-opacity-min" alt="Menu" style="width:100%">
    </div>
  </div>

  <hr>
  
<!-- End page content -->
</div>

<!--Footer starts here--> 
  <!-- Footer -->
  <footer class="w3-padding-64 w3-light-grey w3-small w3-center" id="footer">
    <div class="w3-row-padding">
      <div class="w3-col s4">
        <h4>Contact</h4>
        <p>Questions? Go ahead.</p>
        <form action="/action_page.php" target="_blank" id="contact">
          <p><input class="w3-input w3-border w3-margin" type="text" placeholder="Name" name="Name" required></p>
          <p><input class="w3-input w3-border w3-margin" type="text" placeholder="Email" name="Email" required></p>
          <p><input class="w3-input w3-border w3-margin" type="text" placeholder="Subject" name="Subject" required></p>
          <p><input class="w3-input w3-border w3-margin" type="text" placeholder="Message" name="Message" required></p>
          <button type="submit" class="w3-button w3-block w3-black w3-margin">Send</button>
        </form>
      </div>

      <div class="w3-col s4">
        <h4>About</h4>
        <p class="w3-margin-top"><a href="#">About us</a></p>
        <p class="w3-margin-top"><a href="#">We're hiring</a></p>
        <p class="w3-margin-top"><a href="#">Support</a></p>
        <p class="w3-margin-top"><a href="#">Find store</a></p>
        <p class="w3-margin-top"><a href="#">Shipment</a></p>
        <p class="w3-margin-top"><a href="#">Payment</a></p>
        <p class="w3-margin-top"><a href="#">Return</a></p>
        <p class="w3-margin-top"><a href="#">Help</a></p>
      </div>

      <div class="w3-col s4 w3-justify">
        <h4>Store</h4>
        <p><i class="fa fa-fw fa-map-marker w3-margin-top"></i> Imus St. Imus City Cavite, 4103</p>
        <p><i class="fa fa-fw fa-phone w3-margin-top"></i> 0012345678</p>
        <p><i class="fa fa-fw fa-envelope w3-margin-top"></i> contact@archair.com</p>
        <h4>We accept</h4>
        <p><i class="fa fa-fw fa-cc-amex w3-margin-top"></i> Mastercard</p>
        <p><i class="fa fa-fw fa-credit-card w3-margin-top"></i> Visa</p>
        <br>
        <i class="fa fa-facebook-official w3-hover-opacity w3-large"></i>
        <i class="fa fa-instagram w3-hover-opacity w3-large"></i>
        <i class="fa fa-snapchat w3-hover-opacity w3-large"></i>
        <i class="fa fa-pinterest-p w3-hover-opacity w3-large"></i>
        <i class="fa fa-twitter w3-hover-opacity w3-large"></i>
        <i class="fa fa-linkedin w3-hover-opacity w3-large"></i>
      </div>
    </div>
  </footer>

<!-- End page content -->
</div>
</html>
